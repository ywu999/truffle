
# Uniswap (UNI) https://uniswap.org/

Uniswap is a decentralized exchange that enables peer-to-peer market making. Uniswap is also a cryptocurrency with the symbol UNI. Any one can earn UNI by agreeing to not sell or trade their crypto holdings. The Uniswap platform enables users to trade cryptocurrencies without any involvement by a centralized third party.

The Uniswap blockchain is hosted on the Ethereum platform and governed by UNI holders. Uniswap describes its blockchain as a public good. The Uniswap blockchain is open source, meaning that anyone can view and contribute to the blockchain's code.

The Uniswap platform can support the exchange of any digital token that adheres to the Ethereum technical standard known as ERC-20. Uniswap uses smart contracts, which are enabled by blockchain technology, to function as an automated market maker. Uniswap users can securely create liquidity pools, provide liquidity, and swap a variety of digital assets.

As a decentralized exchange, Uniswap uses a permissionless design. The Uniswap protocol is available for anyone to use, with no ability of the Uniswap platform to selectively restrict access. Anyone who chooses can use Uniswap to trade digital assets, provide liquidity, or create a new market to exchange a new pair of digital assets.3

The automation provided by smart contracts can make trading assets more efficient. Uniswap uses smart contracts to also avoid liquidity issues that traditionally affect centralized exchanges. The elimination of any rent-seeking third party, such as a centralized exchange or financial institution, can also reduce transaction processing fees.

# How Uniswap Works
The Uniswap platform uses blockchain-based smart contracts to facilitate the decentralized trading of many different digital assets. Pairs of digital assets are swapped via liquidity pools, which use smart contracts to automatically rebalance after every trade. The Uniswap blockchain, which functions like an electronic ledger, is continually updated to reflect the trading activity occurring among Uniswap users. By functioning as an exchange without any involvement by a central authority, Uniswap is an automated market maker.

Uniswap operates using the Ethereum platform, which currently uses the proof-of-work operating method. (Ethereum is gradually transitioning to using only the proof-of-stake method.) Proof of work requires vast computing and energy resources, which are used to process transactions and generate new cryptocurrency.

Uniswap users can participate in the decentralized exchange in several ways:

Create new markets: Uniswap users can use smart contracts to create new markets for exchanging new pairs of digital assets.
Swap assets via existing markets: Uniswap can use the platform to swap digital assets, via decentralized markets that have already been created.
Provide liquidity and earn rewards: Uniswap users can provide liquidity by staking—agreeing to not trade or sell—their digital assets. Those who stake their digital currencies on the Uniswap platform are rewarded with UNI.
Participate in Uniswap governance: UNI token holders are empowered to govern the Uniswap platform, with voting power distributed in proportion to users' UNI balances.
Participating in the Uniswap network requires connecting a compatible digital wallet. In addition, since the Ethereum platform collects fees for processing Uniswap transactions, Uniswap users need Ether (ETH) to pay any transaction fees that they incur.

# Advantages and Disadvantages of Uniswap

Enables the decentralized exchange of many digital assets

Smart contracts enable asset trading that may be cheaper and more efficient

Uniswap users can earn UNI by agreeing to not sell or trade their cryptocurrency holdings

Decentralized governance of the Uniswap platform enables anyone to participate

Uniswap only supports the exchange of Ethereum-compatible cryptocurrencies

Proof of work is an energy- and resource-intensive process

Users must own ETH to pay transaction processing fees

Using a decentralized exchange requires having a compatible, self-hosted wallet

# Uniswap vs. PancakeSwap
Uniswap and PancakeSwap are both decentralized exchanges that facilitate the trading of digital assets. Both use tokens—UNI and CAKE, respectively—to incentivize users to provide liquidity.

PancakeSwap and Uniswap operate on different blockchain platforms. Whereas Uniswap uses the Ethereum platform, PancakeSwap uses the Binance Smart Chain. Uniswap supports the exchange of Ethereum-compatible tokens that adhere to the ERC-20 standard, while PancakeSwap enables the exchange of Binance-compatible tokens that comply with Binance's BEP-20 technical standard.

# How can I invest in Uniswap (UNI)?
You can invest in Uniswap by purchasing UNI. The easiest way to buy UNI is through a centralized cryptocurrency exchange such as Coinbase or Kraken.8 You can also purchase UNI through the Uniswap platform, by using another cryptocurrency such as Ethereum.

# Which wallets are compatible with Uniswap?
Uniswap's UNI token is compatible with many digital wallets, including both hardware and software versions. Popular software wallets that can hold UNI include Coinbase Wallet, the MetaMask wallet, and TrustWallet. Hardware wallet options include Ledger and Trezor.

# Can Uniswap be hacked?
Uniswap, as a decentralized exchange that uses blockchain technology, is considered to be secure. Smart contracts on the Uniswap platform are designed to be unalterable, although hacking of smart contracts can generally occur. The Uniswap platform experienced a security breach in 2019 that resulted in $340,000 being lost.
