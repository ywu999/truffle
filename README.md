# Start the truffle

The developer need to install NodeJs, git, and python before starting to use truffle.
```
npm install -g truffle
```

The initial installation might meet with the following issues

```
To address issues that do not require attention, run:
  npm audit fix

To address all issues possible (including breaking changes), run:
  npm audit fix --force
```
To verify that Truffle is installed properly, type truffle version on a terminal. If you see an error, make sure that your npm modules are added to your path.

```
C:\projects\truffle>truffle version
Truffle v5.5.6 (core: 5.5.6)
Ganache v^7.0.3
Solidity v0.5.16 (solc-js)
Node v16.14.0
Web3.js v1.5.3
```

The following command gives you list of all supported solidity versions:
```
truffle compile --list
```
Then you can specify the compile version inside truffle-config.js. For example:

module.exports = {
  networks: {
    ... etc ...
  },
  compilers: {
     solc: {
       version: "0.8.4" 
     }
  }
};

```
mkdir pet-shop

cd pet-shop

truffle unbox pet-shop
```

Create the contract Adoptions under contracts directory

```
truffle compile
```

A migration is a deployment script meant to alter the state of your application's contracts, moving it from one state to the next. For the first migration, you might just be deploying new code, but over time, other migrations might move data around or replace a contract with a new one.

Before we can migrate our contract to the blockchain, we need to have a blockchain running. Ganache is a personal blockchain for Ethereum development you can use to deploy contracts, develop applications, and run tests. If you haven't already, download Ganache and double click the icon to launch the application. This will generate a blockchain running locally on port 7545.

```
truffle migrate
```

The tests can be written in Javascript using the Chai and Mocha libraries or in solidity.

The ganacha is just another network that can be attached to MetaMask with chainId 1337 and RPC URL http://127.0.0.1:7545 . The Metamask's connection with the browser is confusing. The user needs to be careful to use the right account, and disconnect any prior used account.

```
npm run dev
```

# Start the project MetaCoin

In Solidity, a function that doesn't read or modify the variables of the state is called a pure function. It can only use local variables that are declared in the function and the arguments that are passed to the function to compute or return a value.

# Start my own project

```
truffle init ledger
```