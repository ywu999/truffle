// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract Ledger {
    
    mapping(address => uint256) public balances;

    function deposit() public payable {
        balances[msg.sender] += msg.value;
    }

    function withdraw() external {
        uint256 _amount = balances[msg.sender];
        (bool success,) = msg.sender.call{value:_amount}("");
        require(success, "Withdraw failed.");
        balances[msg.sender] = 0;
    }

    function transfer(address to, uint256 amount) external {
        require(balances[msg.sender] >= amount);
        balances[to] += amount;
        balances[msg.sender] -= amount;
    }
}