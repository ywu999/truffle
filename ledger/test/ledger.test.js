const Ledger = artifacts.require("Ledger");
const Drainer = artifacts.require("Drainer");

contract("Ledger", (accounts) => {
    let ledger;
    let drainer;

    before(async () => {
        ledger = await Ledger.deployed();
        drainer = await Drainer.deployed();
    });

    describe("Initiate a ledger", async () => {
        before("Deposit 10 ether using accounts[0]", async () => {
            const options = {from: accounts[0], value:10*10**18};
            await ledger.deposit(options);
        });
      
        it("The accounts[0] should have 10 balance in the ledger.", async () => {
            ledger.balances.call(accounts[0], (err, res) => {
                assert.equal(res, 10*10**18, "The balance will be 10 ether.");
            });
        });

        it("Transfer 1 wei to accounts[1].", async () => {
            await ledger.transfer(accounts[1], 1);
            ledger.balances.call(accounts[1], (err, res) => {
                assert.equal(res, 1, "The balance will be 1 wei.");
            });
        });

        it("Withdraw 1 wei for accounts[1].", async () => {
            await ledger.withdraw({from: accounts[1]});
            ledger.balances.call(accounts[1], (err, res) => {
                assert.equal(res, 0, "The balance will be 0 wei.");
            });
        });
        it("Deposit 1 wei from accounts[1]=>drainer=>ledger", async () => {
            await drainer.deposit({from: accounts[1], value: 1});
            balance = await web3.eth.getBalance(drainer.address);
            assert.equal(balance, 0, "The drainer has no balance.");
            ledger.balances.call(drainer.address, (err, res) => {
                assert.equal(res, 1, "The ledger has 1 wei for " + drainer.address + ".");
            });
        });
        it("Drain the ledge by 10 times drainer's initial investment", async() => {
            let balance0 = await web3.eth.getBalance(ledger.address);
            await drainer.drain();
            balance = await web3.eth.getBalance(drainer.address);
            assert.equal(balance, 10, "The drainer has 10 weis.");
            let balance1 = await web3.eth.getBalance(ledger.address);
            assert.equal(balance0-10, balance1, "The ledger lost 10 weis.")
        });
    });
});
