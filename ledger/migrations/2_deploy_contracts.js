const Ledger = artifacts.require("Ledger");
const Drainer = artifacts.require("Drainer");

module.exports = function(deployer) {
  deployer.deploy(Ledger).then(
    function() {
      return deployer.deploy(Drainer, Ledger.address);
    });
};